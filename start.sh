#!/bin/bash

set -eu -o pipefail

mkdir -p /app/data/plugins

setup() {
    while ! curl --max-time 10 -s --fail -o /dev/null http://localhost:3000/api/session/properties; do
        echo "==> Waiting for metabase to start"
        sleep 3
    done

    echo "==> Setting up admin account"

    if ! setup_token=$(curl --fail -s  'http://localhost:3000/api/session/properties' | jq -r '.["setup-token"]'); then
        echo "==> Could not get setup token"
    fi

    if ! curl --fail -X POST -H 'Accept: application/json' -H 'Content-Type: application/json' --data-raw "{\"token\":\"${setup_token}\",\"prefs\":{\"site_name\":\"Metabase\",\"allow_tracking\":\"false\"},\"user\":{\"first_name\":\"Cloudron\",\"last_name\":\"Admin\",\"email\":\"admin@cloudron.local\",\"password\":\"changeme123\",\"site_name\":\"epicness\"}}" http://localhost:3000/api/setup; then
        echo "==> Could not do automated setup"
    fi

    echo "==> Completed setup"

    touch /app/data/.setup
}

[[ -f /app/data/env ]] && mv /app/data/env /app/data/env.sh

if [[ ! -f /app/data/env.sh ]]; then
  echo "==> Copying default configuration"
  cp /app/pkg/env.sh /app/data/env.sh

  echo "==> Setting encryption key"
  echo "export MB_ENCRYPTION_SECRET_KEY=\"$(openssl rand -base64 32)\"" >> /app/data/env.sh
fi

# https://www.metabase.com/docs/latest/troubleshooting-guide/running.html
export JAVA_OPTS="-XX:+IgnoreUnrecognizedVMOptions -Djava.awt.headless=true -Dfile.encoding=UTF-8 -XX:-UseGCOverheadLimit -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+UseCompressedOops -XX:+UseCompressedClassPointers"

if [[ -f /sys/fs/cgroup/cgroup.controllers ]]; then # cgroup v2
    ram=$(cat /sys/fs/cgroup/memory.max)
    [[ "${ram}" == "max" ]] && ram=$(( 2 * 1024 * 1024 * 1024 )) # "max" means unlimited
else
    ram=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes) # this is the RAM. we have equal amount of swap
fi

if (( ram <= 17179869184 )); then
    ram_mb=$(numfmt --to-unit=1048576 --format "%fm" $ram)
    export JAVA_OPTS="${JAVA_OPTS} -Xmx${ram_mb}"
fi

# Load the end-user configuration
source /app/data/env.sh

echo "==> Loading configuration"
if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    ## Configure LDAP
    export MB_LDAP_ENABLED="true"
    export MB_LDAP_HOST=${CLOUDRON_LDAP_SERVER}
    export MB_LDAP_PORT=${CLOUDRON_LDAP_PORT}
    export MB_LDAP_BIND_DN=${CLOUDRON_LDAP_BIND_DN}
    export MB_LDAP_PASSWORD=${CLOUDRON_LDAP_BIND_PASSWORD}
    export MB_LDAP_SECURITY="none"
    export MB_LDAP_USER_BASE=${CLOUDRON_LDAP_USERS_BASE_DN}
    export MB_LDAP_USER_FILTER="(&(objectclass=user)(|(username={login})(mail={login})))"
    export MB_LDAP_GROUP_SYNC="false"
    export MB_LDAP_ATTRIBUTE_EMAIL="mail"
    export MB_LDAP_ATTRIBUTE_FIRSTNAME="givenName"
    export MB_LDAP_ATTRIBUTE_LASTNAME="sn"
fi

# These are non-overridable by the end-user to ensure a "working" cloudron app
## Disable in-app update checks
export MB_CHECK_FOR_UPDATES="false"
## Set the app URL
export MB_SITE_URL=${CLOUDRON_APP_ORIGIN}
## Configure SMTP
export MB_EMAIL_FROM_ADDRESS=${CLOUDRON_MAIL_FROM}
export MB_EMAIL_SMTP_HOST=${CLOUDRON_MAIL_SMTP_SERVER}
export MB_EMAIL_SMTP_PORT=${CLOUDRON_MAIL_SMTP_PORT}
export MB_EMAIL_SMTP_SECURITY="none"
export MB_EMAIL_SMTP_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}
export MB_EMAIL_SMTP_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}
## Configure postgresql
export MB_DB_TYPE="postgres"
export MB_DB_HOST=${CLOUDRON_POSTGRESQL_HOST}
export MB_DB_PORT=${CLOUDRON_POSTGRESQL_PORT}
export MB_DB_USER=${CLOUDRON_POSTGRESQL_USERNAME}
export MB_DB_PASS=${CLOUDRON_POSTGRESQL_PASSWORD}
export MB_DB_DBNAME=${CLOUDRON_POSTGRESQL_DATABASE}

chown -R cloudron:cloudron /app/data

if [[ ! -f /app/data/.setup ]]; then
    ( sleep 10; setup ) &
fi

echo "==> Migrating Database"
gosu cloudron:cloudron java ${JAVA_OPTS} -jar /app/code/metabase.jar migrate up

echo "==> Starting Metabase"
exec gosu cloudron:cloudron java ${JAVA_OPTS} -jar /app/code/metabase.jar
