FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y openjdk-21-jre-headless && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# https://www.metabase.com/docs/latest/developers-guide/versioning
# renovate: datasource=github-tags depName=metabase/metabase versioning=regex:^0\.(?<major>\d+)\.(?<minor>\d+)\.?(?<patch>\d+)?$ extractVersion=^v(?<version>.+)$
ARG METABASE_VERSION=0.53.5.5

RUN curl -SLf "https://downloads.metabase.com/v${METABASE_VERSION}/metabase.jar" > /app/code/metabase.jar && \
    chown -R cloudron:cloudron /app/code

RUN ln -s /app/data/plugins /app/code/plugins

ADD env.sh start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
