#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global describe, before, after, it, afterEach, xit */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = process.env.TIMEOUT | 30000;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser;
    let app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    });

    after(function () {
        browser.quit();
    });

    async function saveScreenshot(fileName) {
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${fileName.replaceAll(' ', '_')}-${new Date().getTime()}.png`, screenshotData, 'base64');
    }

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');
        await saveScreenshot(this.currentTest.title);
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//div[text()="Sign in"]')).click();

        await browser.wait(until.elementLocated(By.xpath('//h4[contains(text(), "New")]')), TIMEOUT);
    }

    async function checkRootCollection() {
        await browser.get('https://' + app.fqdn + '/collection/root');
        await browser.wait(until.elementLocated(By.xpath('//div[text()="Examples"]')), TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/auth/logout');
        await browser.sleep(3000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no sso
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('wait for 60 seconds', (done) => setTimeout(done, 60000));
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('can logout', logout);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // sso
    it('install app (sso)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('wait for 60 seconds', (done) => setTimeout(done, 60000));

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('check root collection', checkRootCollection);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('check root collection', checkRootCollection);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('check root collection', checkRootCollection);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('check root collection', checkRootCollection);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id com.metabase.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('wait for 60 seconds', (done) => setTimeout(done, 60000));
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('check root collection', checkRootCollection);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });

    it('can admin login', login.bind(null, 'admin@cloudron.local', 'changeme123'));
    it('check root collection', checkRootCollection);
    it('can logout', logout);

    it('can login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});

