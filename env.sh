# Set or override configuration environment variables in this file
# See https://www.metabase.com/docs/latest/operations-guide/environment-variables.html for a complete list of operations
#
# Examples:
# export MB_REPORT_TIMEZONE="US/Eastern"
# export MB_SITE_NAME="My Metabase Site"
# export JAVA_OPTS="${JAVA_OPTS} <custom option>"
export MB_ANON_TRACKING_ENABLED=false

