[0.1.0]
* Initial version

[0.2.0]
* Pre-setup with admin account

[1.0.0]
* Initial stable version

[1.1.0]
* Update metabase to 0.35.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.35.4)
* Security fix for BigQuery and SparkSQL
* Turkish translation is now available again (#12557)
* Better site URL detection when Metabase is run behind a proxy (#12528)
* Changed map tile server URL to HTTPS (#12431)
* Drastically reduced memory usage for streaming large XLSX files — thanks to @sunui for the PR. (#12521)

[1.2.0]
* Update metabase to 0.36.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.0)
* SQL/native query snippets
* Language selection
* Better value labels for line, area, bar, and combo charts
* Reorderable table columns in the Data Model
* A reorganized Data Model section

[1.2.1]
* Update metabase to 0.36.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.1)
* Data Model Metrics shows error if using special combination of Custom Expressions (#13022)
* Dashboards with questions, that user doesn't haven't permissions to, breaks completely (#13005)
* Questions based on Saved Question joined with another Saved Question breaks Notebook (#13000)
* Field filters break SQL-type questions for Redshift data sources in v0.36.0 (#12984)

[1.2.2]
* Update metabase to 0.36.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.2)
* Cannot click some card titles on dashboard to go to question (#13042)

[1.2.3]
* Update metabase to 0.36.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.3)
* Redshift Spectrum: Support for externally linked tables (#7833)

[1.2.4]
* Remove group syncing

[1.2.5]
* Update metabase to 0.36.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.4)
* Handle large Slack channel lists

[1.3.0]
* Update metabase to 0.36.5.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.5.1)
* Remappings should work on broken out fields (#5600)
* Sync fails on Turkish system language on startup and crashes instance (#13102)
* LDAP auth doesn't support IPv6 addresses (#12879)
* Scheduled Jobs triggering on non-existant Database (#11813)
* Cannot use Saved Question, when original question uses "Display values"="Use foreign key" (#10474)
* Can't sync MariaDB System-Versioned tables (#9887)
* Floating-point rounding errors in UI for very large integers (JS stores all numbers as 64-bit floats) (#5816)
* Support unicode parameter names -- all non-ASCII characters are converted to _ when slugifying (#5313)
* Admin Settings page doesn't surface errors when saving settings (#4506)
* Trying to log in via Google Auth with a disabled account should display an error (#3245)

[1.4.0]
* Update metabase to 0.36.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.6)
* Custom Column not working on 0.36.5.1 (#13241)

[1.4.1]
* Update metabase to 0.36.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.7)
* Footer (with export/fullscreen/refresh buttons) on Public/Embedded questions disappears when using Premium Embedding (#13285)
* Postgres sync not respecting SSH tunneling (#8396)
* Partial remediation of OOM errors on syncing large columns/rows (#13288)

[1.4.2]
* Update metabase to 0.36.8
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.36.8)
* Don't add duplicate joins when adding implicit joins (#13520)
* Improved date formatting of Excel/CSV exports (#6012)
* Improved heuristics for giving marking a field's metadata type as State (#2735)
* Update Jetty to address a CVE (#13511)

[1.5.0]
* Update metabase to 0.37.0.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.0.1)
* Cross-filtering
* Linked dashboard filters
* Custom click destinations
* Pick the first day of the week

[1.5.1]
* Update metabase to 0.37.0.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.0.2)

[1.5.2]
* Set Java VM Heap size based on memory limit

[1.6.0]
* Update metabase to 0.37.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.1)
* Table schema sync performance impact (#13650)
* Pie chart shows spinner, when returned measure/value is null or 0 (#13626)
* Wrong day names are displayed when using not-Sunday as start of the week and grouping by "Day of week" (#13604)
* When result row is null, then frontend incorrectly shows as "No results!" (#13571)
* Snowflake tables with a GEOGRAPHY column cannot be explored (#13505)
* Cannot edit BigQuery settings without providing service account JSON again (#13442)
* Sync crashes with OOM on very large columns/row samples [proposal] (#13288)
* 500 stack overflow error on collection/graph API call (#13211)
* Custom Column after aggregation creates wrong query and fails (#12762)
* The expression editor shouldn't start in error mode without any user input (#12140)
* Pulse attachment file sent without file extension (#11879)
* Metric with unnamed Custom Expression breaks Data Model for table (#10844)
* Nested queries with duplicate column names fail (#10511)
* pulse attachment file(question name) Korean support problem (#8410)
* Pulse Bar Chart Negative Values Formatting (#7044)

[1.6.1]
* Update metabase to 0.37.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.2)
* When visualization returns null (No results), then UI becomes broken (#13801)

[1.6.2]
* Update metabase to 0.37.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.3)
* Fix chain filtering with temporal string params like 'last32weeks' (#13876)
* Linked filters breaking SQL questions on v0.37.2 (#13868)
* Running with timezone Europe/Moscow shows Pulse timezone as MT instead of MSK and sends pulses on incorrect time (#13867)
* For dump_to_h2, order fields to dump by ID (#13839)
* For dump_to_h2, remove object count from log output (#13823)

[1.6.3]
* Update metabase to 0.37.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.4)
* Error in Query: Input to aggregation-name does not match schema (#14080)
* Exports always uses UTC as timezone instead of the selected Report Timezone (#13677)
* Between Dates filter behaves inconsistently based on whether the column is from a joined table or not (#12872)

[1.6.4]
* Update metabase to 0.37.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.5)
* Linked filters breaking SQL questions on v0.37.2 (#13868)
* Embedding loading slow (#13472)
* Cannot toggle off 'Automatically run queries when doing simple filtering and summarizing' (#13187)

[1.6.5]
* Update metabase to 0.37.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.6)

[1.6.6]
* Update metabase to 0.37.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.7)

[1.6.7]
* Update metabase to 0.37.8
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.8)

[1.7.0]
* Use latest base image 3.0.0

[1.7.1]
* Update metabase to 0.37.9
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.37.9)
* Includes (undisclosed) important security fixes

[1.8.0]
* Update metabase to 0.38.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.38.0)
* Pivot tables - At long last, Metabase now has honest-to-goodness pivot tables, with multiple levels of grouping and pivots, subtotals and grand totals, and collapsing and expanding rows
* Dashboard subscriptions
* Waterfall charts
* A new layout for collections

[1.8.1]
* Update metabase to 0.38.0.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.38.0.1)

[1.8.2]
* Update metabase to 0.38.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.38.1)
* Breadcrumbs can be confusing (the current one "seems" clickable when it's not) (#14879)
* Difficult to see which cells has "Click behavior" vs normal behavior (#13548)

[1.8.3]
* Update metabase to 0.38.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.38.2)
* Data model not showing PostgreSQL tables when they are partitioned (#15049)
* Regression with URL links (#12366)
* Sync task never runs on subsequent databases if one of the databases is down/etc #(14817)

[1.8.4]
* Update metabase to 0.38.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.38.3)
* Overflow text on Ask a question page (#15439)
* Frontend load issue: SMTP Email (#12692)
* Add Bitcoin as a unit of currency (#11406)
* Line chart dots don't have cursor: pointer when hovering (#5056)

[1.8.5]
* Update metabase to 0.38.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.38.4)
* Not possible to position Y-axis if there's only one series (#13487)
* Tooltip on unaggregated data does not show summed value like the visualization (#11907)
* For a new Custom column, I can set Style to "Currency", but cannot choose the Unit of Currency (#11233)

[1.9.0]
* Update metabase to 0.39.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.39.0)
* Add missing "is" assertions to various tests (#15254)
* Custom Expression autocomplete operator selection is appended to what was typed (#15247)
* Custom Expression formula starts with high cursor placement on Firefox (#15245)
* Custom Expression filter not setting the "Done" button to current state of the formula until onblur (#15244)
* Dashboard Subscription Filters: Set Parameter Values (#15084)
* Normalize queries in URL fragments on FE (#15079)

[1.9.1]
* Update metabase to 0.39.0.1

[1.9.2]
* Update metabase to 0.39.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.39.1)
* Table with multiple Entity Key columns incorrectly filtering on "Connected To" drill-through (#5648)

[1.9.3]
* Update metabase to 0.39.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.39.2)
* Not possible to select pinned collection item using checkbox (#15338)
* Collection tree loader causes UI jump (#14603)

[1.9.4]
* Update metabase to 0.39.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.39.3)
* Feature flag causes problems with Number filters in Native query (#16218)
* Revoking access to users in multiple groups does not correctly cleanup GTAP (#16190)
* ExpressionEditor loses value when user resizes browser window (#16127)
* ExpressionEditor loses value when user clicks away from associated name input (#16126)
* Filter dropdown not working for non-data users, when field has 300+ distinct values. (#16112)
* Tooltip only shows first Y-axis value when X-axis is numeric and style is Ordinal/Histogram (#15998)

[1.9.5]
* Update metabase to 0.39.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.39.4)
* Lookup ip address for new login email off thread
* Deref with timeout

[1.10.0]
* Update metabase to 0.40.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.40.0)
* A new and improved way to add questions to your dashboards
* Vastly improved reordering and resizing
* A more intuitive way to start new questions from SQL results
* Improved data selection

[1.10.1]
* Update metabase to 0.40.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.40.1)
* Remove Multi-Release from manifest (#17026)
* Global search suggestions dropdown appears behind the dataset search widget when starting a simple question (#16897)
* Clean up the user-facing strings for coercion options (#16547)

[1.10.2]
* Update metabase to 0.40.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.40.2)

[1.10.3]
* Update metabase to 0.40.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.40.3)
* Run-overlay not going away on GUI question (#17514)
* Keep collection_id of dashboard subscriptions in sync with same field on dashboard (#17586)
* Dashboard causes scrollbars to constantly go on/off depending on viewport size (#17229)
* Serialization --mode skip incorrectly updates some objects (#16954)
* Serialization crashes on dump if there's no collections (#16931)
* Serialization: Cannot load into empty/blank target (#16639)
* Clicking the column formatting button when the sidebar is already open should correctly open that column's formatting sidebar (#16043)

[1.10.4]
* Update metabase to 0.40.3.1

[1.10.5]
* Update metabase to 0.40.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.40.4)
* Dashboard filter autocomplete not working with mixed Search/Dropdown when dropdown has a null value (#17659)
* Not possible to delete Dashboard Subscription unless dashboard is in root collection (#17658)
* Possible to not input recipient of Subscription, which will then cause blank screen (#17657)
* Valid Email settings disappear on save, but re-appear after refresh (#17615)
* Unable to click "Learn more" on custom expression (#17548)

[1.10.6]
* Update metabase to 0.40.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.40.5)
* GeoJSON URL validation fix (#17991)
* Grid map causes frontend reload or blank screen, when hovering the grids if there is no metric (#17940)
* Cannot create more than 2 metrics, when UI language is Turkish - screen goes blank or displays "Something went wrong" (#16323)
* Visualizations with more than 100 series just shows a blank chart (#16000)
* Data point values uses formatting Style of first serie on all series (#13095)

[1.11.0]
* Update metabase to 0.41.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.0)
* Revamped Subscriptions and Alerts
* Fully redesigned permissions page (and a new permission level)
* Stay organized at scale
* Fully-formatted result downloads
* A better experience for questions with errors
* Find slow queries and optimize them

[1.11.1]
* Update metabase to 0.41.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.1)
* Better approach for column ordering in exports (#18450)
* Tools for fixing errors problems with Postgres semantics of limits (blank display of error table) (#18432)
* Raise `MB_DB_CONNECTION_TIMEOUT_MS` to 10000 as default (#18354)
* Allow caching of fonts and images (#18239)

[1.11.2]
* Update metabase to 0.41.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.2)
* Frontend crashes when opening admin database permissions page (#18829)
* Cannot access Notebook for questions that uses a Custom Column as joining column (#18818)
* Requests to GET /api/card/123 is making slow queries on larger instances (#18759)
* BigQuery can cause conflict with some column names like source (#18742)
* "Verify this question" button is shown even when content moderation feature is not enabled (#18740)
* New BigQuery driver with "Project ID (override)" defined causes different Field Filter references (#18725)
* Dashboard subscription send by Email fails with xlsx attachements (#18724)
* Textbox markdown links on images difficult to click (#18641)
* Some questions with old field dimensions or changed columns cannot be viewed, because of Javascript loop (#18630)
* Multi-column join interface defaults binning for numeric fields causing incorrect results (#18589)
* Sandboxing queries fails with caching is enabled (#18579)
* Changing redshift db details leads to closed or broken resource pool (#18499)
* Audit visualizations does not show correct information, when there's more than 1000 aggregated dates (#17820)
* "Set up your own alert" text needs padding (#17701)

[1.11.3]
* Update metabase to 0.41.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.3)

[1.11.4]
* Update metabase to 0.41.3.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.3.1)

[1.11.5]
* Update metabase to 0.41.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.4)
* Addresses the CVE-2021-44228 security vulnerability by updating log4j2 to 2.15.0

[1.11.6]
* Update metabase to 0.41.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.5)
* Upgrade Log4j to 2.16.0 (#19371) - this addresses CVE-2021-45046
* XLSX export does not respect "Separator style" (#19006)
* One cannot change any of the LDAP Settings once it's been initially setup (#18936)
* Custom Expression coalesce is using wrong field reference when nested query (#18513)
* Custom Expression case is using wrong field reference when nested query (#17512)
* Dashboard causes permission error, when "Click Behavior" linking to dashboard/question without access (#15368)
* Site URL validation too strict, doesn't accept underscore (#14491)

[1.11.7]
* Update metabase to 0.41.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.41.6)
* Broken layout on Data Model page on locales with longer text (#16267)
* Specific visualization_settings can cause empty export (#19465)
* Cannot change Field Filter reference to another schema/table if viewing the question directly (#19451)
* Upgrade Log4j to 2.17.1 (#19420)
* Names of schemas or namespaces in the Data Model should wrap (#18584)

[1.12.0]
* Update metabase to 0.42.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.42.0)

[1.12.1]
* Update metabase to 0.42.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.42.1)

[1.12.2]
* Update metabase to 0.42.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.42.2)

[1.12.3]
* Update metabase to 0.42.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.42.3)

[1.12.4]
* Update metabase to 0..42.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.42.4)

[1.13.0]
* Update metabase to 0.43.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.0)

[1.13.1]
* Update metabase to 0.43.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.1)

[1.13.2]
* Update metabase to 0.43.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.2)

[1.14.0]
* Update metabase to 0.43.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.3)
* Slack setup doesn't work if channels cache was recently cleared (#23251)
* Custom Expression field is not respecting the modal size (#23223)
* Relative filter with a lower "Starting from" period is confusing (#23099)
* Cannot print large dashboards (multi page) (#23098)
* Sync of JSON columns fails in schemas where schema names contains system characters (#23027)
* Sync of fields fails, when table name contains uppercase characters (#23026)
* Preview function in Notebook does not respect the columns selected on base source (#23023)
* Viewing Metadata for a Model and cancelling can result in error, and further in blank screen (#23022)
* Do not offer "Explore results" unless the database supports it (#22822)

[1.14.1]
* Update metabase to 0.43.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.4)

[1.14.2]
* Update metabase to 0.43.4.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.4.1)
* Set a timeout for isValidTimeout (#24289)

[1.14.3]
* Update metabase to 0.43.4.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.43.4.2)

[1.15.0]
* Update metabase to 0.44.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.0)

[1.15.1]
* Update metabase to 0.44.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.1)
* Fix incorrect hash for Advanced fieldvalues (#24740)
* Confusing UX after performing password reset (#24700)
* Changing existing filters in Simple GUI will replace other existing filters (#24664)
* Cannot set caching duration to less than a second (#24657)
* Correct spelling of pulse setting notification-retry-randomizaion-factor (#24653)

[1.15.2]
* Update metabase to 0.44.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.2)
* Flex data selector trigger wrapping (#25131) (#25132)

[1.15.3]
* Update Metabase to 0.44.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.3)
* Slack files channel can be confusing after initial setup (#20936)
* SessionStorage can become filled causing the frontend to throw errors until browser restart (#25312)
* Filters incorrectly showing as linked to all fields on combined charts (#25248)
* First created question not in Saved Questions inside Data Selector until page reload (#25144)
* Cannot change email From-name and Reply-to-address when environment variables are used for other settings (#25057)
* Column filters not working on multi-aggregation (#25016)
* Tooltip periods are not displayed nicely on SQL questions (#25007)
* Can't change filters via 'Show filters' (#24994)
* Bubble size not consistent across multiple series (#24936)
* A hidden Database Sync doesn't allow descriptions for tables or fields to be edited (#24900)
* Newly created Model not an option when creating a new question (#24878)
* Dashboard filter does not show Search widget when trying to workaround other filter issues (#24741)
* Chinese weekdays in calendar widget are incorrect (#22871)
* Field filter linked to Boolean column causes an error when refreshing a public question (#22591)

[1.15.4]
* Update Metabase to 0.44.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.4)
* Bump Postgres driver to 42.5.0 (#25544)
* Inform admins if their account is in bad standing (#25161)
* Bump MinaSSHD to 2.9.1 (#25112)
* Envar to disable custom maps / GeoJSON (#19189)
* Metabase Prometheus Exporter (#12377)
* When LDAP is enabled but password login is disabled, login doesn't work (#25661)
* 404 on loading models when instance is configured in another language (#25537)
* Loading spinners in filters don't appear anymore (#25322)
* MYSQL disable JSON unfolding doesn't work (#25068)
* MySQL unfolding of JSON fails for boolean and bigint attributes (#24720)
* Aggregation in Models causes the column name to incorrectly be parsed down to questions based on that Model (#23248)
* Changing filters on Simple question drops anything after first aggregation (#14193)
* Changing filters on Simple question drops aggregated filters (#11957)
* Drill through / underlying records action doesn't respect filters in metric definitions (#6010)

[1.15.5]
* Update Metabase to 0.44.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.5)
* Slack request error leads to sensitive authorization token being logged (#25849)
* Synchronized tables that were initially marked as cruft has their initial_sync_status as incomplete (#25291)
* Cannot drill-through "View these ..." when aggregated results are filtered (#13504)

[1.15.6]
* Update Metabase to 0.44.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.44.6)
* Google Sign-in fails because of unannounced change by Google (#26184)
* Load in serialization with --mode update fails on upserts (#26043)
* Between() in custom filter expression switches min and max upon adding new filter (#26025)
* Collection list fails to display if there's an invalid parameter in a question (#25543)
* Inconsistent behavior when sending certain payloads to /permissions/graph (#25221)

[1.16.0]
* Update Metabase to 0.45.0
* Use Cloudron base image 4.0.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.45.0)

[1.16.1]
* Update Metabase to 0.45.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.45.1)
* Added support for the datetime-diff function for Snowflake
* Athena documentation
* Fix incorrect datetime-diff results for bigquery on leap years
* Fix inconsistence temporal extract for timestamptz columns for h2
* Serdes v2: Correctly (de)serialize references to segments and native snippets

[1.16.2]
* Update Metabase to 0.45.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.45.2)
* Relieve db pressure on api/health check (#27192)
* Update token check URL (#27036)
* Disable Athena driver IAM role-based auth in Cloud (#27014)
* Improve collection breadcrumbs with embedding parameters (#26838)
* unset MB_API_KEY for notify endpoint could use some feedback (#15157)
* Log4j2.xml file not read on 0.45 (#27497)
* Cannot select fields for double aggregated metrics, and existing sections gets removed on change (#27462)
* Background color on hover for items in data picker looks off (#27449)
* Static viz fails when there's an unused column returned (#27427)
* "Something's gone wrong" error when moving between dashboards that has/hasn't filters (#27356)
* Changing sorting does not apply for X-axis anymore (#27279)
* Number filter with 0 (zero) as default value shows as empty in the widget (#27257)
* Static viz fails when date formatting contains day (#27105)
* "Replace missing values with" set to Zero or Nothing causes the line to become zeroed/hidden (#27063)
* Static viz fails when date formatting is abbreviated (#27020)
* Snowflake sync is consuming more resources than necessary (#26054)
* Click Behavior cannot handle absolute URLs outside of Metabase (#25953)
* Avoid updating dashboard cards unless filter changes (#25914)
* Using filter Contains on dashboard is case-insensitive, but on drill-through to question becomes case-sensitive (#25908)
* BigQuery data type BIGDECIMAL is recognized as mixed string/numeric (#25508)
* Text filter over a custom column in simple embedding expects a number (#25473)
* Comma separated filter value would not hand over from dashboard to question (#25374)
* Embedding Dashboard with Locked parameters does not allow numeric values (#25031)
* Column name auto-classification not working for `_latitude` (#24041)
* Corrupted question shows "Something's gone wrong" with no more info, and breaks "Add series" modal on the dashboard, and breaks Archive page (#15222)

[1.16.3]
* Update Metabase to 0.45.2.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.45.2.1)
* Dashboard subscription settings interface reveals list of recipients to sandboxed users
* Users can view data they don’t have privileges to view by adding themselves to dashboard subscriptions created by users with more data privileges

[1.16.4]
* Update Metabase to 0.45.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.45.3)
* Send "threads blocked" via Prometheus exporter (#28014)
* Make educational messaging in native questions less distracting (#27797)
* Fix "Powered by Metabase" badge partially invisible with transparent theme (#27796)

[1.17.0]
* Update Metabase to 0.46.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.0)
* Metabase can now write to your database with actions. Admins: enable Model Actions on your Postgres or MySQL database. Create a new action from the + New menu, or from the new Model details page.
* Upgraded MongoDB driver, including a smarter schema sync process to handle variable keys, support for models, fast joins, custom expressions, additional aggregations, and more.
* Improved sync and fingerprint performance, reducing the load on your database and costs for your data warehouse.
* New dropdown options in dashboard filters.
* A new model details page: view a list of questions using the model, the model's schema, and its actions.
* Smarter tooltips are more useful in multi-series charts with totals and percentages, time series with days of the week, and more
* More static viz improvements for subscriptions and alerts.
* New link cards for dashboards.
* New visualization type: Detail (displays a row as an easy-to-read card).
* The Clickhouse partner driver is here and available for Metabase Cloud.
* Deprecation notice: we're deprecating Google Analytics 3, and will remove the driver from our future releases after it stops accepting new events (in July of 2023). We don't have plans of supporting GA4. (But link to an article explaining how to use it with BigQuery.)

[1.17.1]
* Update Metabase to 0.46.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.1)
* Fix implicit actions not created in a deterministic order (#29605)
* Currency list: Add Mauritania ouguiya (MRU) as currency (#29545)
* drill-through not working correctly in legend for stacked charts (#29840)
* SSO mapping: users are removed from groups that are not mapped (#29718)
* Date Filter (Specific Dates) doesn't show first days of the month (#29692)
* Big xslx file downloaded from BigQuery are corrupted in v0.45.3 and were not in v0.45.1 (#29118)
* Infinite loop, logging at warn level, filling disk for a GCP BigQuery connection (#28868)
* Dashboard sticky filter incorrectly positioned, when moving between dashboards (#26230)
* Unable to properly filter on an aggregation (#22230)

[1.17.2]
* Update Metabase to 0.46.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.2)
* Include checklist task to migrate off h2 (#30126)
* Users with No Access to the Collection Our Analytics are not able to define a dropdown list from models/questions (#30355)
* MongoDB Custom Column SUM with arithmetic operation producing wrong MongoQL (#30262)
* Race condition after deleting the Sample Database (#30152)
* [Snowflake] Syntax error: unexpected "?" when using filters inside window frames, or inside INTERVAL syntax (#30103)
* Revision history erratically loads and removes historic changes (#30030)
* "People can pick" ->multiple values in dashboards and custom dropdown filter don't go together (#29997)
* Changing column order on model view then almost saving without preview breaks model with custom columns (#29951)
* Metric tooltip description gets cut (#29862)
* Metadata is required when trying join on an SQL based questions (#29795)
* Adding more than one field filter blows up the frontend in Mongo and MySQL (#29786)
* Filter values no longer work inside single quoted strings (#29690)
* Serialization v1 errors when trying to load an instance with actions (#29528)
* Cannot get into a card from a dashboard on "no self-service" permissions (#29076)
* "First day of the week" not changing calendar widget on filters (#13899)

[1.17.3]
* Update Metabase to 0.46.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.3)
* This release fixes a security issue where native models enabled people without SQL permissions to create and edit SQL snippets. For more info, see our security advisory.
* Could not read SSL key file when attempting to connect to Postgres DB with SSL using PKCS8 DER key file (#30717)
* Tables in reference section are not ordered alphabetically (#30727)
* "Bulk select" checkbox selects all collection items (pinned items included) (#30571)
* Invalid query generated in v46 with BigQuery (#30648)
* Field custom mappings not working on (public) embedding (#29346)
* Custom mappings are broken (#29347)
* Fix custom mapping doesn't work on dashboard (#30217)
* User session interferes with signed embedding if non-admin user has sandboxed permissions (#30535)
* Incorrect query on nested questions in BigQuery (#30610)
* Snowflake sync is consuming more resources than necessary (#26054)
* Can't turn off "Also change sub-collections" settings and apply permission changes only for the selected collection (#30494)
* dump command fails to serialize toucan2 records (#29322)
* When changing collection permissions "include subcollections" doesn't work (#20911)
* Chart Settings allows you to drag fields when there is only 1 item in the list (#28973)
* Link for Cloud firewall docs renders incorrectly (#30317)
* SmartScalars should aggregate the same way as Scalars (#25637)

[1.17.4]
* Update Metabase to 0.46.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.4)
* This release removes the activity feed from Metabase. The `/activity` page displayed recent things people had made in Metabase: new questions, dashboards, etc. This activity page, however, failed to account for group permissions when displaying these items, so anyone could see what anyone else was making. We may reintroduce the activity page in the future, but we're removing it for now to address this metadata visibility issue.
* Link Cards that link to internal entities should open the object in the same iFrame in Full-App embedding (#30851)
* Activity feed leaks metadata (#30695)
* Mongodb won’t work if version does fall into the “semantic-version-gte” pattern (Percona) (#29678)
* 46 regression: datetimeAdd is apparently supposed to allow non-integer amounts (#29638)
* If you bring the sample dataset back then go to 'Browse Data' it does not appear unless you refresh the page (#29496)

[1.17.5]
* Update Metabase to 0.46.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.5)
* MySQL default timeout needs to be increased (#31102)
* Make the "save" modal context-aware (#28802)
* When setting/resetting password, sign in with your new password shouldn't make you retype your password again (#5063)
* Cannot run native queries against MongoDB unless explicitly selecting the table (#15946)
* Add to Dashboard/Save Question Menu doesn't respect Collection Alphabetical order as in Main Menu (#31425)
* Downloading data from cards in dashboards does not respect download limit (#31393)
* Respect alphabetical order of collections in "Move" modal (#31294)
* Attempting to upload RSA private key (PEM PK8) to connect to Snowflake fails with Private key provided is invalid or not supported (#31273)
* Dump broken on 46.x on instances with more than 65K categorical fields (#31230)
* Question with source query containing aggregation and sorting by that aggregation fails on mongodb (#30874)
* Upgrade to 1.46 fails due to invalid state in permissions table (#30872)
* Reverting to an earlier version of a dashboard creates an erroneous event in revision history (#30776)
* Model caching not working anymore in 0.46.x with MySQL if query takes more than 29 seconds (#30723)
* Question breaks after trying to create pivot table and edit on aggregated results that are not grouped (#30711)
* Not able to Enable/Disable Actions when Choose when syncs and scans happen is Enabled (#30699)
* Native editor buttons are incorrectly rendering on top of model metadata screen (#30680)
* Unable to summarize models (#30112)
* Sporadic "we're a little lost..." pop-up when scrolling up and down my query after expanding a row (#30039)
* Basic actions (CUD) display order is non-deterministic (#29593)
* Do not offer to save database changes if there were none (#29388)
* There is no confirmation when running an action without parameters in dashboards (#28981)
* SSO does not respect Metabase subpath (#28650)
* `MB_SESSION_TIMEOUT` behavior does not match documentation (#28454)
* Doing "Zoom in" from dashboard drops fields from joined tables (#27380)
* Google Sign-in ignores "Notify admins of new SSO users" toggle (#24381)
* Drill-through to questions with add columns (implicit join) causes no filtering to be defined (#23293)
* Better error message/validation if SAML Keystore has no private key (#19500)

[1.17.6]
* Update Metabase to 0.46.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.6)
* Remove task snowplow event (#31156)
* Incorrect query on nested questions in BigQuery (#30610)
* Snowflake won't sync without password (user using only private key) (#30376)
* Don't auto-run sql questions when there's an edit (#30165)
* Error custom column "only binary :- is supported" (#29858)

[1.17.7]
* Update Metabase to 0.46.6.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.6.1)

[1.17.8]
* Update Metabase to 0.46.6.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.6.2)

[1.17.9]
* Update Metabase to 0.46.6.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.6.4)

[1.17.10]
* Update Metabase to 0.46.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.46.7)
* Prevent open redirects for JWT/SAML (#32816)
* Certain invalid URL causes error response from Jetty (#23277)
* Very Inefficient query causing Metabase DB load to go to 100% [v0.46.6.4] (#32737)
* LDAP Attribute sync no longer works (#32243)
* "Expected native source query to be a string, got: clojure.lang.PersistentArrayMap" on nested queries (#32121)
* Snowflake won't sync without password (user using only private key) (#30376)
* Resizing columns in pivot tables in serialization breaks the card on load (#30130)

[1.18.0]
* Update Metabase to 0.47.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.0)
* cancels ongoing cards when saving a dashboard
* refactor: change from stub to spy so that we can also see in the inspector that the request has been cancelled
* test that the data will eventually be displayed

[1.18.1]
* Update Metabase to 0.47.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.1)
* Switch pulse email sending to use bcc instead of sending a seperate email (#33604)
* Show new syncing tables as greyed out in data reference page (#33152)
* Bottom corner radius on buttons looks weird on new dashboard grid (#31587)
* Update default and min size constraints for dashboard cards (#31408)

[1.18.2]
* Update Metabase to 0.47.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.2)
* Clear cache when performing version upgrades (#33457)
* Performance improvement in notebook editor (#33676) (#12378)
* Metabase will not let you update the cert if it was rotated (#33452)
* Some table columns are unable to be rendered (#33279)
* Download results (.csv, .json, .xlsx) request does not include subpath when deploying on subpath (#33244)

[1.18.3]
* Update Metabase to 0.47.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.3)
* Use HTTPS when getting version-info (#34310)
* Better deduplication for entities loader api queries (#34174)
* Fix CSV uploads aren't possible when native query editing is disabled (#33990)
* bad gateway 502 for API via reverse proxy (POST request ignores 'location' v0.47.2) (#33910)
* Provide a heads up when Metabase is upgraded with what’s new (#33165)
* Can't apply category filters when a question has grouping and aggregation (#34279)
* Summarizing or sorting in query editor after Exploring results in a SQL question and creating a Custom Column breaks query editor (#34247)
* Other count displays incorrect value when Language set to Arabic or Arabic (Saudi Arabia) (#34236)
* Date filters - inconsistent filter values displayed when a question is opened from a filtered dashboard (#34129)
* UI suggests it's possible to delete an archived collection (#33996)
* CSV uploads aren't possible when native query editing is disabled (#33971)
* Search bar font size is too small on Safari (#33930)
* Summarizing by custom date column does not allow date grouping (#33504)
* Custom ranges in conditional formatting don't work (#33229)
* After hiding column in table settings on question based on another question, all joined columns unchecked in notebook editor when still selected (#32374)
* Unable to add joined columns in table settings after removing through notebook editor on nested Question (#32373)
* Inputting some values into Whitelabel "Landing Page" can cause the entire frontend to break (#18004)

[1.18.4]
* Update Metabase to 0.47.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.4)
* Implement unreturnedConnectionTimeout in c3p0 in an env var (#34377)
* load-from-h2 fails on master with Command failed with exception: ERROR Clearing default entries created by Liquibase migrations... (#34505)
* DashboardTab missing from copy command (#34411)
* Dashboard loses effective filter values when auto-apply is turned off (#34382)
* We don't seem to be updating certificates when updating the connection (#33116)
* Hardly visible expand icon on collection hover (#31394)

[1.18.5]
* Update Metabase to 0.47.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.5)
* Fix SQL autocomplete is hard to use because keywords are shown instead of tables and columns (#34601)
* Replace java-time namespace with java-time.api (#34506)
* Snowflake Database Setting allows you to input small caps role but breaks connection afterwards (used to work for both in 1.46.8) (#34829)
* 404 when downloading results on embedded questions on sub-paths (#34634)
* Splitting multidimension questions that only have 1 metric (#34618)
* The slack token is logged in plain text (#34616)
* Event Icon for "Balloons" is a cake (#34581)

[1.18.6]
* Update Metabase to 0.47.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.6)
* Error: Field X is not present in the Query Processor Store. (#34950)
* Selecting filter on date, then going back and selecting expression results in error (#34794)
* Auto pivot is freezing query editor on high cardinality fields (#34677)
* JavaScript TypeError when removing dashboard filter (#33826)
* Can't go back with browser back button from "Table metadata" (#33784)
* Large column names should wrap in the sidebar (#31086)

[1.18.7]
* Update Metabase to 0.47.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.7)
* Disable auto-commit (enable transactions) for read-only connections (#35268)
* Adding check for column before getting friendly name (#34761)
* Support subtracting intervals (#23423)
* Graphing chart with date filter at 24 months is broken (#35129)
* Serialization v2 export doesn't include parameter_card entity_ids which breaks the import (#35097)
* Dashboard reversion should skip cards that have been deleted or archived (#34884)
* Serialization v2 duplicates entities loaded with serialization v1 on old versions (#34871)

[1.18.8]
* Update Metabase to 0.47.8
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.8)
* Fix exporting dashboard with configured click_behavior (#35913)
* Fixing bad performance due to massive pprint statements (#35895)
* Fix serdes of click_behavior on dashcards (#35717)
* avoid calling fetchCardData in undoRemoveCardFromDashboard for virtual cards (#35545)
* restore correct position when undoing dashcard tab movement or deletion (#35502)
* Respect "enable nested queries" setting and database's "nested queries" feature (#35490)
* Add an option in the /admin/settings/email to opt-in to CC recipients (#34293)
* Export throws a Unexpected character ('b' (code 98)) (#35880)
* Export throws a NullPointerException (#35855)
* API returns 500 instead of 404 when visiting an non-existing card (#35692)
* A big warning that if you use CSV uploads in the sample database in the cloud, your data will disappear (#35638)
* Serialization import doesn't remove dashboardcards in the target instance (#35420)
* Filters blink when there are many lines of filters in an embedded context with headers=false (#35078)
* Users with read-only permissions to collections are shown "edit question" option in dashboard cards (#35077)
* Text weirdly wrapped in column dropdown in Polish language (#35047)
* Pivot viz fails on a nested question (#35025)
* '-' sign misplaced in shortened visualization of currency type (#34242)
* Log why Serialization v2 fails when trying to export a dashboard that contains cards in outside collections (#33882)
* Click behavior: dashboard and saved question links are not preserved during export / import (#33699)
* Don't offer to create models or use nested questions on 3rd party drivers which don't support it (#33401)
* e.some is not a function breaks the FE on the filter modal (#32985)
* Linked fiters won't work if you use a question to populate filter values (#31218)
* Disabled "Nested Queries" setting is not always respected (#28908)
* Filter dropdown not showing remapped values on the dashboard (#21528)

[1.18.9]
* Update Metabase to 0.47.9
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.9)
* Upload configuration is not deserialized (#36232)
* Remove divider in QueryBuilder/ViewHeader when it's not needed (#36128)
* Handle CVE in commons-compress and org.json/json (#36106)
* Syncs from Redshift failing since version 0.47.7 (#36040)
* Dashboards with pages sometimes appear blank when opened from bookmarks (#35959)
* Export throws a Unexpected character ('b' (code 98)) (#35880)
* Use the official MySQL latest image for drivers tests (#35641)
* Serialization will fail if there's a snippet on destination with an already existent name but different entity id (#35629)
* Blank strings in CSV uploads should be NULL in MySQL (#35606)
* Restore correct position when undoing dashcard tab movement or deletion (#35502)
* Dashboard subscription attachment does not take into account the dashboardcard visualization settings (#35437)
* Pivot viz fails on a nested question (#35025)
* Static/Public embeds fail to load dashboard when one of the cards fails to load due to missing filter value. (#34954)
* Exporting a child collection won't export the parent collection (#33900)
* Queries not getting cancelled in MongoDB when leaving the page (#32372)
* Creating/editing database connection with SSH tunnel fails silently, when SSH port is not filled out (#32234)
* Serialization won't respect column sorting in pivot tables (#30134)
* Pivot table with 2 group by's gets loaded incorrectly on serialization (#30133)
* Dashboard parameter without field mapping have double borders (#27365)
* Tables render slowly (#25274)
* Serialization: Table columns with "Display values"="Use foreign key" not always saved during dump (#15848)

[1.19.0]
* Rename env to env.sh
* Generate 32 byte secret key

[1.19.1]
* Update Metabase to 0.47.10
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.47.10)
* Make ambiguous matches a warning in lib.equality (#36210)
* Fix card filter connected to a field not causing an update (#36206)
* Export fails with a confusing error message when the target directory is not writable (#36468)
* Admin Settings / People list is incorrect for users promoted from group manager to administrator (#36412)
* drop-entity-ids fails when dashboards have tabs (#36365)

[1.20.0]
* Update Metabase to 0.48.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.0)
* Introduce Metabase analytics Collection (#35434)
* Add Finnish language (#33477)
* Clean up the pulse page (#36811)
* Allow spaces in datetime strings for CSV uploads (#35648)
* Check if inactive tables are being used by questions before deleting them (#35615)
* Make `SESSION_COOKIE_SAMESITE` a setting (#35021)
* Optimize global search 20% (#35367)
* Auto-wire up dashboard field filters (#35147)
* Add percentage type to metadata (#35124)
* Make `MB_AUDIT_MAX_RETENTION_DAYS` truncate data from the `view_log` or `audit_log` table and enforce new rules (#34985)
* Explain why linked filters don't work and prevent accidentally disabling them (#34604)
* Prevent users from adding questions from personal collections to dashboards in public collections (#34733)
* Let people move cards between dashboard tabs (#34367)
* Allow Linking dashcards to dashboard tabs (#34779)
* Update dashboard performance 48 (#34721)
* Allow users to link dashcard to dashboard tab in Click Behavior sidebar (#34781)

[1.20.1]
* Update Metabase to 0.48.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.1)
* .48.0 upgrade: js front error regression on some questions (#36895)
* Cannot override location for /plugins/instance_analytics (#36845)
* Dashboard filter values causing card errors since update to v48 (#36841)
* Column mapping can overflow the sidebar when editing model metadata (#36163)
* Verified question icon doesn't update (appear/disappear) unless page reloads (#32505)
* Whitelabel landing page string allows whitespace which leads to "page not found" (#25482)

[1.20.2]
* Update Metabase to 0.48.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.2)
* Fix issue with model index search (#37308)
* Fix card loading state in different tabs when parameters change (#37134)
* show error and disable save button when create metric fails (#36874)
* CSV upload support for Redshift (#36166)
* Ability to customize table row limit in subscriptions (#32165)
* Subtracting two temporal values should yield an interval (#37263)
* Caching not working for (all) mongo questions (#37140)
* Malli warning in metabase.models.recent-views/update-users-recent-views! (#37030)
* Confusing copy in confirm modal when changing table data access to Sandboxed (#34911)
* We're not signaling when a card is loading if you change the filters on a dashboard and switch tabs (#33767)

[1.20.3]
* Update Metabase to 0.48.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.3)
* Restore GET /api/dashboard endpoint (#37455)
* Validate json settings during init (#36182) (#37313)
* Fixing case of invalid graph.dimension (#37279)
* CSV uploads: support more date formats (#37078)
* Updating honeysql alias in dev ns (#36758)
* Auditv2 prevented startup (#37480)
* [Serialization] Skip Metabase Analytics and Custom reports collections when exporting content (#37453)
* Embeded dashboard takes longer and longer to display. (#37289)
* Slack integration failure on dashboard subscription (#37266)
* Multi series questions won't get imported when using serialization (#37232)
* Fix hsql import in dev.clj (#36757)
* Invalid json for env variable settings breaks site (#36182)
* Setting all reports to hide with no results breaks dashboard subscription (#34777)
* Timestamp column be converted twice in 2-layer questions query (#33861)
* Number Visualisation show wrong field in dashboard subscription (#32362)
* Snowflake driver won't use JDBC options (#27856)
* Funnel charts with "Month of Year" type dimension and ordering can't render in Static Viz (#26944)

[1.20.4]
* Update Metabase to 0.48.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.4)

[1.20.5]
* Update Metabase to 0.48.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.5)
* Fixing static-viz exports of saved x-ray dashboards

[1.20.6]
* Update Metabase to 0.48.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.6)
* do the unzip sooner, still hash from the file system
* java-time -> java-time.api

[1.20.7]
* Update Metabase to 0.48.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.7)
* We should never run get-field-values on a dashboard load if the field does not have field values in the metabase_fieldvalues table (#38826)
* Date filtering change between v0.48.3 and v0.48.4. looks timezone related (#38248)
* Relative date filter with time works in Field Filters different for SQL variables than for GUI (#38037)
* Change native SQL datetime range field filters to be inclusive of the end minute (#33492)
* Date filters giving inconsistent results, depends on filter mode (#30454)
* Update group-by-week-test to use database timezone id (#38805)

[1.20.8]
* Update Metabase to 0.48.8
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.48.8)

[1.21.0]
* Update Metabase to 0.49.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.0)

[1.21.1]
* Update Metabase to 0.49.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.1)

[1.21.2]
* Update Metabase to 0.49.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.2)
* Handle invalid file drop (#40690)
* Sort official collections first in API endpoints (#40598)
* Increase rate limit of actions from 1 to 10 per second (#40527)
* Order of collection should be the same as the collection sidebar (#39965)
* Show more billing info on license and billing settings page (#38497)

[1.21.3]
* Update Metabase to 0.49.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.3)

[1.21.4]
* Update Metabase to 0.49.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.4)
* asynchronously load libraries for png and pdf exports (#41036)
* Fix "instace analytics" typo (#36596)
* Signed Embedding without Title will remove all the tabs of a dashboard as well (#41195)
* SQL generated for datetime "is empty" filter on MSSQL is not correct (#41158)
* Casting integer to Datatime via Table Metadata makes column unusable (#41122)
* "No method in multimethod 'coerce-temporal' for dispatch value: [nil java.time.LocalDateTime]" when evaluating null datetimes (#40885)
* Bigquery: Can't search field values for required partition tables (#40673)
* Iframe embedding overflow widgets issue (#40660)
* GUI model joining native ones break with column not found error (#40252)
* Waiting for result tab title contains js code (#40051)
* State of the FE gets lost on detail views when there are multiple joins to the same tables (#39477)
* Manually choosing Axis in chart settings will break subscription (#38839)
* Embedding height is not dynamic (#37437)
* View Details: "is connected to:" uses wrong PK for query with join (#34070)

[1.21.5]
* Update Metabase to 0.49.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.5)
* When exporting to CSV (or JSON or XLSX) from an embedded report, the file only contains 'Unknown parameter :format_rows.' (#41240)
* Downloading results of an embedded question (via iframe) fails because of "Unknown parameter :format_rows" (#40959)
* Notebook query preview exists even before the data source is selected (#40608)
* Incorrect results from SQL query referencing saved question with more than 1,048,575 rows (#24969)
* Fix naming a filter offset or limit breaks dashboards (#40821)

[1.21.6]
* Update Metabase to 0.49.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.6)
* CSV upload support for Redshift (#36166)
* Add INFO log for each table being fingerprinted (#41365)
* Tab query param not applied in embed (#41440)
* Borders not respecting bordered=false parameter for static embedding (#41393)
* Progress visualizations don't show, except on dashboards (#41243)
* Wrong suggested join condition when joining 2 models (#41202)
* Advanced option to turn off syncing field values doesn't work (#40715)
* Progress visualisation not working, gives an error (#40658)
* Text filters shown as input box in embed instead as dropdown (#37914)
* SIGTERM during migration does not cleanly abort migration (#37866)

[1.21.7]
* Update Metabase to 0.49.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.7)
* Fix not sending necessary HTTP headers when downloading results (#41633)
* Fix invisible user items in search filters (#41604)
* docs - add redshift to list of dbs that support uploads (#41594)
* Fix Embedded dashboard parameters parse runtime error (#41545)
* Links to metabase URLs should open in the same window in link cards (#41508)
* Date format "January 31, 2018" missing day in csv download (#41639)
* Default (formatted) CSV Export Returns "Week #" instead of Date (#41492)
* Leading zero in filter value in embedded dashboard breaks it (#41483)
* Archived models continue to be cached (#41318)
* Migration doesn't like rows with null characters (#40835)
* Primary Key missing filter options (#40665)
* If a user is a member of multiple groups in the IdP, the user property is comma separated and we default to "all users" (#40517)
* Date Filtering Bug in v0.49.1 Docker Image with Japanese Locale Settings (#40493)
* Metabase renders the result but the query does not finish in the DB (#39018)
* Illegal hex characters in escape (%) pattern - error at index 0 in: R6 (#38966)
* Cannot connect to new MongoDB source or sync existing databases (#38013)
* Overall link situation (#30891)

[1.21.8]
* Update Metabase to 0.49.8
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.8)
* Faster driver/describe-database for Redshift (#41068)
* Support SAML and JWT logins at the same time (#18554)
* Snowflake temporal extract operators like hour-of-day do not respect report timezone (#42073)
* Slack integration uses a deprecated API (#41890)
* Snowflake always returns times truncated second precision, we should fix this for tests (#41866)
* Snowflake UNIX timestamps interpreted as being in report timezone rather than UTC (#41863)
* Issues connecting to Snowflake using RSA Keypair Authentication (#41852)
* Fix queries being re-run because of broken parameters comparison (#41659)
* Date format "January 31, 2018" missing day in CSV download (#41639)
* Uploaded model not showing latest rows appended to the underlying table (#41630)
* Serialization import should not fail on hidden .yaml files (#41567)
* Default (formatted) CSV Export Returns "Week #" instead of Date (#41492)
* Admin checklist indicates more progress than actual due to audit items (#41335)
* Date Filtering Bug in 49.1 Docker Image with Japanese Locale Settings (#40493)
* Switching between tabs reruns queries even if filters are not modified (#39863)
* Google SSO with Interactive Embedding does not allow you to download CSV (#39848)
* Filtering by a Specific Date in Snowflake will bring inaccurate results (#39769)
* Use scientific notation on frontend multipliers (#29974)
* Data mismatch between custom question and raw sql query with Redshift (#19910)
* Quarterly Date Filter different on Dashboard than on Question (#11643)
* Snowflake JDBC driver incorrectly converts JDBC dates with a timezone (#8804)

[1.21.9]
* Update Metabase to 0.49.9
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.9)
* Adds Key Authentication Option to SSH Tunneling Docs (#42206)
* Allow drivers to not automatically add a PK column for CSV uploads (#42359)
* Upgrade Redshift driver to v26 (#42450)
* Allow using urls from other columns in link templates (#42438)
* Misc documentation improvements
* Async streaming response error when Jetty SSL enabled (#13771, #15806)
* Embedding preview not behaving like Metabase Application when filter is locked and linked (#41635)
* Overflow problem in filter modal sidebar (#42386)
* Upgrade failing from instance using outdated 3rd party drivers (#42319)
* Unable to sync dynamic tables in Snowflake (#42312)
* Azure AD SAML and SLO logout error (#41600)
* Bigquery unable to sync materialized views (#42008)
* Unable to filter on IDs identified as primary key in Mongo (#42010, #40770)
* Fix MySQL with multiple most recent revision (#41374)

[1.21.10]
* Update Metabase to 0.49.10
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.10)
* Allow disabling /api/health logging via env var (#35338)
* Documentation improvements (#42481, #42485, #42488, #42492, #42490, #42439, #42522)
* Cannot import dashboards with circular click behavior (#38665)
* Pie chart tooltip "Other" group produces NaN% (#42458)
* Pie chart sometimes disappears when switching from notebook editor (#42367)
* Columns excluded from the unformatted option when downloading (#42500)

[1.21.11]
* Update Metabase to 0.49.11
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.11)
* Make JSON parsing more efficient on the JSON unfolding feature (#34798)
* Fix dashboard subscription's Japanese file name of attachment (#41669)
* Fix metadata loading issues after tabs (#42655)
* Fix migration issues related to a missing encryption key (#42612) (#42611)
* Realize database transient rows in after-select hook before calling driver.u/features (#42744)

[1.21.12]
* Update Metabase to 0.49.12
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.12)
* Fix report_card.collection_preview in v49 (#42950)
* Correct type for report_card.collection_preview (#42919)
* backported "fix: border appearing on night theme if bordered=false" (#42898)
* fix: border appearing on night theme if bordered=false (#41514)
* Pivot table becomes broken if a breakout clause is added without visualizing the question (#42697)
* Clicking existing filter breaks question based on model (#41783)
* Cannot duplicate or save questions when moving from 42 to 49 (#40600)

[1.21.13]
* Update Metabase to 0.49.13
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.13)

[1.21.14]
* Update Metabase to 0.49.14
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.49.14)

[1.22.0]
* Update Metabase to 0.50.0
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.0)

[1.22.1]
* Update Metabase to 0.50.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.1)

[1.22.2]
* Update Metabase to 0.50.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.2)

[1.22.3]
* Update Metabase to 0.50.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.3)
* Users with No Access to the Our Analytics collection are not able to use the Query Editor for Saved Questions or Models (#44071)
* Comma in quoted string during CSV upload: CSV error (unexpected character: ,) (#44060)

[1.22.4]
* Update Metabase to 0.50.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.4)

[1.22.5]
* Update Metabase to 0.50.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.5)
* v0.50.x - "scale: Timeseries" not working with Oracle database - "invalid time zone in DateTimeFormat(): +00:00" (#44128)
* capitalize the ‘U’ in user interface (#44196)
* Fix spaces in dashboard move toast (#44192)
* Fix tool-tip values not being scaled in waterfall charts (#44180)

[1.22.6]
* Update Metabase to 0.50.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.6)
* Disable SQL analysis by default + turn off all QueryField backfills (#44346)
* Rework measure search performance events (#44300)
* Remove FE code that checks enable-query-caching (#44250)

[1.22.7]
* Update Metabase to 0.50.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.7)

[1.22.8]
* Update Metabase to 0.50.8
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.8)

[1.22.9]
* Update Metabase to 0.50.9
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.9)

[1.22.10]
* Update Metabase to 0.50.10
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.10)

[1.22.11]
* Update Metabase to 0.50.11
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.11)

[1.22.12]
* Update Metabase to 0.50.12
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.12)

[1.22.13]
* Update Metabase to 0.50.13
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.13)

[1.22.14]
* Update Metabase to 0.50.14
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.14)

[1.22.15]
* Update Metabase to 0.50.15
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.15)

[1.22.16]
* Update Metabase to 0.50.16
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.16)

[1.22.17]
* Update Metabase to 0.50.17
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.17)

[1.22.18]
* Update Metabase to 0.50.18
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.18)
* handle double-quotes in table and schema name in privileges query (#46268) (#46274)

[1.22.19]
* Update Metabase to 0.50.18.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.18.1)
* dismiss permissions modal regardless of network request

[1.22.20]
* Update Metabase to 0.50.18.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.18.3)

[1.22.21]
* Update Metabase to 0.50.18.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.18.4)

[1.22.22]
* Update Metabase to 0.50.19
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.19)

[1.22.23]
* Update Metabase to 0.50.19.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.19.1)

[1.22.24]
* Update Metabase to 0.50.19.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.19.2)

[1.22.25]
* Update Metabase to 0.50.19.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.19.3)

[1.22.26]
* Update Metabase to 0.50.19.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.19.4)

[1.23.0]
* Update Metabase to 0.50.20.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.20.3)

[1.23.1]
* Update Metabase to 0.50.20.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.20.4)

[1.23.2]
* Update Metabase to 0.50.20.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.20.5)

[1.23.3]
* Update Metabase to 0.50.20.6
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.20.6)

[1.23.4]
* Update Metabase to 0.50.20.7
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.20.7)

[1.23.5]
* Update Metabase to 0.50.21.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.21.2)

[1.23.6]
* Update Metabase to 0.50.21.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.21.3)

[1.23.7]
* Update Metabase to 0.50.21.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.21.4)

[1.24.0]
* Update Metabase to 0.50.22
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.22)

[1.24.1]
* Update Metabase to 0.50.22.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.22.1)

[1.24.2]
* Update Metabase to 0.50.22.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.22.2)

[1.25.0]
* Update Metabase to 0.50.24.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.24.4)

[1.25.1]
* Update Metabase to 0.50.24.5
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.24.5)
* Correctly handle :lib/internal-remap columns in :result_metadata (#47587) (#47788)
* Fixes #45938.
* Remapping is handled at the top level of the query, and should be ignored for inner queries. This PR hides it from results_metadata on source queries.
* It also fixes the interaction with large-int-id logic to make big integers JS-safe.

[1.26.0]
* Update Metabase to 0.50.25
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.25)
* Tidy up CSV encoding detection (#47809) (#47813)

[1.26.1]
* Update Metabase to 0.50.25.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.25.1)
* fix static visualizations dashcard settings merging
* cleanup, fix types
* drop an empty file

[1.26.2]
* Update Metabase to 0.50.25.2
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.25.2)

[1.26.3]
* Update Metabase to 0.50.25.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.25.3)

[1.26.4]
* Update Metabase to 0.50.25.4
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.25.4)

[1.26.5]
* Update Metabase to 0.50.26.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.26.1)
* Fix text alignment for dashboard text card (#47540) (#47978)

[1.26.6]
* Update Metabase to 0.50.27
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.27)

[1.26.7]
* Update Metabase to 0.50.27.1
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.27.1)

[1.26.8]
* Update Metabase to v0.50.27.3
* [Full changelog](https://github.com/metabase/metabase/releases/tag/v0.50.27.3)

[1.26.9]
* update metabase to v0.50.27.4
* [full changelog](https://github.com/metabase/metabase/releases/tag/v1.50.27.4)

[1.27.0]
* update metabase to v0.50.28.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.28.1)

[1.27.1]
* update metabase to v0.50.28.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.28.2)

[1.27.2]
* update metabase to v0.50.28.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.28.2)

[1.27.3]
* update metabase to v0.50.28.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.28.4)

[1.27.4]
* update metabase to v0.50.28.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.28.5)

[1.27.5]
* update metabase to v0.50.28.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.28.6)

[1.27.6]
* update metabase to v0.50.29
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.29)

[1.28.0]
* update metabase to v0.50.30
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.30)

[1.28.1]
* update metabase to v0.50.31
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.31)

[1.28.2]
* update metabase to v0.50.31
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.31)

[1.28.3]
* update metabase to v0.50.31.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.50.31.1)

[1.29.0]
* update metabase to v0.51.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1)

[1.29.1]
* Update metabase to v0.51.1.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.1)

[1.29.2]
* Update metabase to v0.51.1.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.2)

[1.29.3]
* Update metabase to v0.51.1.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.3)

[1.29.4]
* Update metabase to v0.51.1.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.4)

[1.29.5]
* Update metabase to v0.51.1.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.5)

[1.29.6]
* Update metabase to v0.51.1.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.6)

[1.29.7]
* Update metabase to v0.51.1.7
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v1.51.1.7)

[1.29.8]
* Update Metabase to 0.51.1.8
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.1.8)

[1.30.0]
* Update Metabase to 0.51.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.2)
[1.30.1]
* Update Metabase to 0.51.2.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.2.1)

[1.30.2]
* Update Metabase to 0.51.2.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.2.2)

[1.30.3]
* Update Metabase to 0.51.2.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.2.3)

[1.30.4]
* Update metabase to 0.51.2.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.2.4)

[1.31.0]
* Update metabase to 0.51.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3)
* Make sure a new table gets table-level block if any other tables have table-level block ([#&#8203;49509](https://github.com/metabase/metabase/issues/49509))
* Faster sync on MySQL ([#&#8203;49010](https://github.com/metabase/metabase/issues/49010))
* Add userAgent to Databricks connections ([#&#8203;49350](https://github.com/metabase/metabase/issues/49350))
* Deprecate ModifyQuestion sdk component ([#&#8203;49716](https://github.com/metabase/metabase/issues/49716))
* fix(sdk): Fix visualization default size ([#&#8203;49672](https://github.com/metabase/metabase/issues/49672))
* fix(sdk): put a bandage on the flashing error on static question in strict mode ([#&#8203;49659](https://github.com/metabase/metabase/issues/49659))
* Generated CLI component has inconsistent styles after the style leak fix ([#&#8203;49652](https://github.com/metabase/metabase/issues/49652))
* Ask for database credentials as a first step in the embedding cli ([#&#8203;49485](https://github.com/metabase/metabase/issues/49485))

[1.31.1]
* Update metabase to 0.51.3.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3.1)

[1.31.2]
* Update metabase to 0.51.3.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3.2)

[1.31.3]
* Update metabase to 0.51.3.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3.3)

[1.31.4]
* Update metabase to 0.51.3.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3.4)

[1.31.5]
* Update metabase to 0.51.3.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3.5)

[1.31.6]
* Update metabase to 0.51.3.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.3.6)

[1.32.0]
* Update metabase to 0.51.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.4)

[1.32.1]
* Update metabase to 0.51.4.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.4.1)

[1.32.2]
* Update metabase to 0.51.4.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.4.2)

[1.32.3]
* Update metabase to 0.51.4.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.4.3)

[1.32.4]
* Update metabase to 0.51.4.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.4.4)

[1.32.5]
* Update metabase to 0.51.4.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.4.5)

[1.33.0]
* Update metabase to 0.51.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5)
* Allow "allow" properties for iframes inside a dashboard ([#&#8203;50020](https://github.com/metabase/metabase/issues/50020))
* Add an env var to disable pivoted exports ([#&#8203;50347](https://github.com/metabase/metabase/issues/50347))
* GeoJSON asset file should follow the metabaseInstanceUrl when in embedding sdk ([#&#8203;50132](https://github.com/metabase/metabase/issues/50132))
* Detect and emit TypeScript component files for TS projects in the embedding cli ([#&#8203;50094](https://github.com/metabase/metabase/issues/50094))
* Improve the import path suggestions printed into the console in the embedding cli ([#&#8203;50093](https://github.com/metabase/metabase/issues/50093))
* Change CreateQuestion's API, implementation and improve default behaviour on saving questions ([#&#8203;50082](https://github.com/metabase/metabase/issues/50082))
* Ability to render custom question views when clicking on a dashcard in InteractiveDashboard ([#&#8203;49718](https://github.com/metabase/metabase/issues/49718))
* Add more padding to list of published embeds ([#&#8203;50268](https://github.com/metabase/metabase/issues/50268))
* Center the content of the "Onboarding" page ([#&#8203;50368](https://github.com/metabase/metabase/issues/50368))
* Update videos for the "Onboarding" page ([#&#8203;50367](https://github.com/metabase/metabase/issues/50367))
* Unify look of banners ([#&#8203;50356](https://github.com/metabase/metabase/issues/50356))
* Hide Admin menu items in airgapped instances ([#&#8203;50295](https://github.com/metabase/metabase/issues/50295))
* Skip connection check when creating/updating channel ([#&#8203;49661](https://github.com/metabase/metabase/issues/49661))
* ...

[1.33.1]
* Update metabase to 0.51.5.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.33.2]
* Update metabase to 0.51.6.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.33.3]
* Update metabase to 0.51.6.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.33.4]
* Update metabase to 0.51.6.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.34.0]
* Update metabase to 0.51.7
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.34.1]
* Update metabase to 0.51.7.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.35.0]
* Update metabase to 0.52.1.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)
* [Sankey chart](https://www.metabase.com/docs/v0.52/questions/sharing/visualizations/sankey) / User Flow Visualization ([#&#8203;11888](https://github.com/metabase/metabase/issues/11888), [#&#8203;29671](https://github.com/metabase/metabase/issues/29671), [#&#8203;50550](https://github.com/metabase/metabase/issues/50550), [#&#8203;50611](https://github.com/metabase/metabase/issues/50611))
* Grey colors for bar, line and other charts ([#&#8203;49130](https://github.com/metabase/metabase/issues/49130))
* Language localization in static embedding with hash parameter `#locale=xx` ([#&#8203;8490](https://github.com/metabase/metabase/issues/8490), [#&#8203;49770](https://github.com/metabase/metabase/issues/49770), [#&#8203;50313](https://github.com/metabase/metabase/issues/50313), [#&#8203;50199](https://github.com/metabase/metabase/issues/50199))
* Show "view-only" badge when the question is not editable ([#&#8203;48791](https://github.com/metabase/metabase/issues/48791))
* Connect dashboard filters to columns from any query stage ([#&#8203;46519](https://github.com/metabase/metabase/issues/46519))
* Verified dashboards ([#&#8203;48954](https://github.com/metabase/metabase/issues/48954), [#&#8203;49398](https://github.com/metabase/metabase/issues/49398))
* Enhanced search: speed, match, and ranking improvements ([#&#8203;40964](https://github.com/metabase/metabase/issues/40964), [#&#8203;50684](https://github.com/metabase/metabase/issues/50684), [#&#8203;50778](https://github.com/metabase/metabase/issues/50778))
* Use plain string types for entity ids instead of NanoID in question component in embedding SDK([#&#8203;50563](https://github.com/metabase/metabase/issues/50563))
* Add missing props from InteractiveDashboard to StaticDashboard for consistency in the sdk ([#&#8203;50490](https://github.com/metabase/metabase/issues/50490))

[1.35.1]
* Update metabase to 0.52.1.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.35.2]
* Update metabase to 0.52.1.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.36.0]
* Update metabase to 0.52.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)
* Streamline embed flow when embedding is off ([#&#8203;50842](https://github.com/metabase/metabase/issues/50842))
* Fix edge cases in New Search around model filtering ([#&#8203;50937](https://github.com/metabase/metabase/issues/50937))
* New and improved Examples collection ([#&#8203;50829](https://github.com/metabase/metabase/issues/50829))
* Ensure user count is fetched within token status memoization ([#&#8203;50813](https://github.com/metabase/metabase/issues/50813))
* Show /api/search errors in Browse models and Browse metrics ([#&#8203;50713](https://github.com/metabase/metabase/issues/50713))
* Non-admin Onboarding Checklist ([#&#8203;50576](https://github.com/metabase/metabase/issues/50576))
* Deprecate alert and pulse apis ([#&#8203;50453](https://github.com/metabase/metabase/issues/50453))

[1.36.1]
* Update metabase to 0.52.2.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.51.5.1)

[1.36.2]
* Update metabase to 0.52.2.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.2.2)

[1.36.3]
* Update metabase to 0.52.2.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.2.3)

[1.36.4]
* Update metabase to 0.52.2.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.2.5)

[1.36.5]
* Update metabase to 0.52.2.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.2.6)

[1.37.0]
* Update metabase to 0.52.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.3)

[1.37.1]
* Update metabase to 0.52.3.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.3.1)

[1.37.2]
* Update metabase to 0.52.3.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.3.2)

[1.37.3]
* Update metabase to 0.52.3.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.3.3)

[1.37.4]
* Update metabase to 0.52.3.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.3.5)

[1.37.5]
* Update metabase to 0.52.3.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.3.6)

[1.37.6]
* Update metabase to 0.52.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4)
* Add basic auth capabilities to Druid JDBC ([#&#8203;45728](https://github.com/metabase/metabase/issues/45728))
* Add percentages to Sankey tooltip ([#&#8203;51193](https://github.com/metabase/metabase/issues/51193))
* Add logging for linked filter queries ([#&#8203;46888](https://github.com/metabase/metabase/issues/46888))
* perf: Add fastpaths to url? and email? predicates ([#&#8203;51522](https://github.com/metabase/metabase/issues/51522))
* \[openapi] reduce amount of oneOf/allOf we generate in documentation ([#&#8203;51495](https://github.com/metabase/metabase/issues/51495))
* \[Clean-up] Allow collection clean up banner to be dismissible ([#&#8203;51434](https://github.com/metabase/metabase/issues/51434))
* Send pulse asynchronously with a small pool size ([#&#8203;51380](https://github.com/metabase/metabase/issues/51380))
* Remove translation for slack error ([#&#8203;51375](https://github.com/metabase/metabase/issues/51375))
* Add prometheus metric for search errors ([#&#8203;51357](https://github.com/metabase/metabase/issues/51357))
* Co-ordinate Search Index creation and rotation between instances ([#&#8203;51279](https://github.com/metabase/metabase/issues/51279))
* Cannot Filter on UUID ID Columns in SQL Server / Azure Synpase ([#&#8203;51189](https://github.com/metabase/metabase/issues/51189))
* Filtering on a multi stage question in MongoDB results in a non-working query ([#&#8203;50832](https://github.com/metabase/metabase/issues/50832))
* Dashboards with tables are nearly invisible in full screen dark mode ([#&#8203;51524](https://github.com/metabase/metabase/issues/51524))
* Exporting to Formatted CSV or JSON Rounds Numeric Fields When Using "Multiply by a Value" ([#&#8203;49439](https://github.com/metabase/metabase/issues/49439))
* PDF exports of dashboards are cut off ([#&#8203;51491](https://github.com/metabase/metabase/issues/51491))
* Bar chart has overlapping bars ([#&#8203;51521](https://github.com/metabase/metabase/issues/51521))
* Wrong line is selected when you click on view details ([#&#8203;51301](https://github.com/metabase/metabase/issues/51301))
* Command palette should not show you admin actions if you do not have access to them ([#&#8203;49729](https://github.com/metabase/metabase/issues/49729))
* Deadlock on the app db (52) ([#&#8203;51316](https://github.com/metabase/metabase/issues/51316))
* Pruning query execution log can time out ([#&#8203;51432](https://github.com/metabase/metabase/issues/51432))
* New Metrics API misses human-friendly text metric definition ([#&#8203;51303](https://github.com/metabase/metabase/issues/51303))

[1.37.7]
* Update metabase to 0.52.4.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4.1)

[1.37.8]
* Update metabase to 0.52.4.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4.2)

[1.37.9]
* Update metabase to 0.52.4.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4.3)

[1.37.10]
* Update metabase to 0.52.4.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4.4)

[1.37.11]
* Update metabase to 0.52.4.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4.6)

[1.37.12]
* Update metabase to 0.52.4.7
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.4.7)

[1.38.0]
* Update metabase to 0.52.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5)
* Make JWT Identity Provider URI Setting optional ([#&#8203;51826](https://github.com/metabase/metabase/issues/51826))
* Grid color in the EditableDashboard component should be themeable in the sdk ([#&#8203;47567](https://github.com/metabase/metabase/issues/47567))
* Small a11y improvements ([#&#8203;51577](https://github.com/metabase/metabase/issues/51577))
* Graph lines are disappear after mouse over ([#&#8203;51383](https://github.com/metabase/metabase/issues/51383))

[1.38.1]
* Update metabase to 0.52.5.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5.1)

[1.38.2]
* Update metabase to 0.52.5.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5.2)

[1.38.3]
* Update metabase to 0.52.5.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5.3)

[1.38.4]
* Update metabase to 0.52.5.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5.4)

[1.38.5]
* Update metabase to 0.52.5.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5.5)

[1.38.6]
* Update metabase to 0.52.5.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.5.6)

[1.39.0]
* Update metabase to 0.52.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.6)

[1.39.1]
* Update metabase to 0.52.6.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.6.1)

[1.39.2]
* Update metabase to 0.52.6.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.6.2)

[1.39.3]
* Update metabase to 0.52.6.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.6.3)

[1.40.0]
* Update metabase to 0.52.7
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.7)

[1.40.1]
* Update metabase to 0.52.7.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.7.1)

[1.40.2]
* Update metabase to 0.52.7.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.7.2)

[1.40.3]
* Update metabase to 0.52.7.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.7.3)

[1.40.4]
* Update metabase to 0.52.7.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.7.4)

[1.41.0]
* Update metabase to 0.52.8.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.8.1)
* Support aggregation functions inside expressions in aggregation ([#&#8203;52611](https://github.com/metabase/metabase/issues/52611))
* Don't automatically persist/cache models in Pro/Enterprise ([#&#8203;52209](https://github.com/metabase/metabase/issues/52209))
* \[SDK] Update Next.js compat layer's components list ([#&#8203;52511](https://github.com/metabase/metabase/issues/52511))
* Cmd+Click on search results should open the link in a new tab ([#&#8203;35902](https://github.com/metabase/metabase/issues/35902))
* Fix loader flicker and double scrollbars with the entity framework in Admin -> Data model -> Segments ([#&#8203;52411](https://github.com/metabase/metabase/issues/52411))
* Move weight updates to a PUT ([#&#8203;52582](https://github.com/metabase/metabase/issues/52582))
* Pass full_stacktrace in /api/ee/serialization/import ([#&#8203;52580](https://github.com/metabase/metabase/issues/52580))
* Make `/auth/sso` return a json error on oss instead of a 404 html page ([#&#8203;51972](https://github.com/metabase/metabase/issues/51972))
* ...

[1.41.1]
* Update metabase to 0.52.8.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.8.3)

[1.41.2]
* Update metabase to 0.52.8.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.8.4)

[1.41.3]
* Update metabase to 0.52.8.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.8.5)

[1.41.4]
* Update metabase to 0.52.9.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.9.1)
* feat(sdk): Add option to remove dashboard footer ([#&#8203;52555](https://github.com/metabase/metabase/issues/52555))
* Disable device login emails for authentication via the Embedding SDK ([#&#8203;52196](https://github.com/metabase/metabase/issues/52196))
* Handle legacy secrets double encoded with base64 ([#&#8203;52836](https://github.com/metabase/metabase/issues/52836))
* Initialize zeros for labeled counter metrics ([#&#8203;52834](https://github.com/metabase/metabase/issues/52834))
* Feature exclusion for Materialize ([#&#8203;52045](https://github.com/metabase/metabase/issues/52045))
* Old permissions deprecation email is triggered many times after upgrading to 52.8 ([#&#8203;53054](https://github.com/metabase/metabase/issues/53054))
* "Display as Link" Configuration in Model Metadata doesn't Work ([#&#8203;51925](https://github.com/metabase/metabase/issues/51925))
* Drill-through actions for the wrong column are displayed when grouping by fields with null values ([#&#8203;49740](https://github.com/metabase/metabase/issues/49740))
* Wrong breadbrumbs when opening a metric ([#&#8203;47006](https://github.com/metabase/metabase/issues/47006))
* Update action has different value for timestamp columns when view and update ([#&#8203;32840](https://github.com/metabase/metabase/issues/32840))
* Pivoted xlsx exports - pivot table fields are named as 'Sum' instead of column name ([#&#8203;51342](https://github.com/metabase/metabase/issues/51342))
* Non-pivoted Excel downloads do not work with "Multiply by a number" (scale) ([#&#8203;50518](https://github.com/metabase/metabase/issues/50518))
* Custom Separator Style Isn't respected on Dashboard when Tile Size Reduced ([#&#8203;51075](https://github.com/metabase/metabase/issues/51075))
* There's no way to change the series color in multi-metric no-breakout chart ([#&#8203;46750](https://github.com/metabase/metabase/issues/46750))
* Mini Bar Chart for Percentage field shows full bar for values less than 100% ([#&#8203;34257](https://github.com/metabase/metabase/issues/34257))
* Column set as category allows numeric aggreagations but not Styling and Formatting ([#&#8203;30431](https://github.com/metabase/metabase/issues/30431))
* Font fallback on row chart legend is inconsistent with --mb-default-font-family ([#&#8203;53007](https://github.com/metabase/metabase/issues/53007))
* Limit the amount of logs we store in our ring buffer for the UI ([#&#8203;52537](https://github.com/metabase/metabase/issues/52537))
* We should trim the dashboard tab names ([#&#8203;50797](https://github.com/metabase/metabase/issues/50797))

[1.41.5]
* Update metabase to 0.52.9.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.9.2)

[1.41.6]
* Update metabase to 0.52.9.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.52.9.3)

[1.42.0]
* Update metabase to 0.53.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.1)
* Update Java to 21

[1.42.1]
* Update metabase to 0.53.1.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.1.1)

[1.43.0]
* Update metabase to 0.53.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.2)
* Remove blue color from identifiers in the SQL editor ([#&#8203;53479](https://github.com/metabase/metabase/issues/53479))
* Clarify that the embedding CLI does not work with existing Metabase instances ([#&#8203;51384](https://github.com/metabase/metabase/issues/51384))
* Don't cache query results for impersonated users ([#&#8203;53489](https://github.com/metabase/metabase/issues/53489))
* Perf & correctness fixes for preemptive caching ([#&#8203;53328](https://github.com/metabase/metabase/issues/53328))
* long question name breaks save question modal layout ([#&#8203;53231](https://github.com/metabase/metabase/issues/53231))
* Switcher between table and viz is off center and jumps when switching ([#&#8203;53116](https://github.com/metabase/metabase/issues/53116))
* FK-mapped filter list disappears when using aggregated questions on a model ([#&#8203;53059](https://github.com/metabase/metabase/issues/53059))
* Saving metric referencing itself should be forbidden ([#&#8203;52385](https://github.com/metabase/metabase/issues/52385))
* Self-joining a question and saving it by replacing the original question results in circular dependency and corrupts the question beyond recovery ([#&#8203;35980](https://github.com/metabase/metabase/issues/35980))
* "Reset to Default" on Viz Settings Doesn't Indicate that Chart Name will Change ([#&#8203;52769](https://github.com/metabase/metabase/issues/52769))
* Pivot table click behavior config misses columns ([#&#8203;52339](https://github.com/metabase/metabase/issues/52339))
* Remove button for conditional formatting rule collapses if rule has many columns ([#&#8203;49931](https://github.com/metabase/metabase/issues/49931))

[1.43.1]
* Update metabase to 0.53.2.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.2.1)

[1.43.2]
* Update metabase to 0.53.2.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.2.2)

[1.43.3]
* Update metabase to 0.53.2.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.2.3)

[1.44.0]
* Update metabase to 0.53.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3)

[1.44.1]
* Update metabase to 0.53.3.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3.1)

[1.44.2]
* Update metabase to 0.53.3.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3.2)

[1.44.3]
* Update metabase to 0.53.3.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3.3)

[1.44.4]
* Update metabase to 0.53.3.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3.4)

[1.44.5]
* Update metabase to 0.53.3.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3.5)

[1.44.6]
* Update metabase to 0.53.3.6
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.3.6)

[1.45.0]
* Update metabase to 0.53.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.4)

[1.45.1]
* Update metabase to 0.53.4.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.4.1)

[1.45.2]
* Update metabase to 0.53.4.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.4.2)

[1.45.3]
* Update metabase to 0.53.4.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.4.3)

[1.45.4]
* Update metabase to 0.53.4.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.4.4)

[1.46.0]
* Update metabase to 0.53.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.5)

[1.47.0]
* Update base image to 5.0.0

[1.47.1]
* Update metabase to 0.53.5.1
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.5.1)

[1.47.2]
* Update metabase to 0.53.5.2
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.5.2)

[1.47.3]
* Update metabase to 0.53.5.3
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.5.3)

[1.47.4]
* Update metabase to 0.53.5.4
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.5.4)

[1.47.5]
* Update metabase to 0.53.5.5
* [Full Changelog](https://github.com/metabase/metabase/releases/tag/v0.53.5.5)

