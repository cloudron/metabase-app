## About

Metabase is the easy, open source way for everyone in your company to ask questions and learn from data.

### Ask your own questions

* Explore on your own - Easily summarize and visualize your data without ever writing a line of SQL or having to wait on a coworker.

* Quick and easy lookup - Browse or search through your tables, then filter things down to find just what you need.

* Visualize results - Move from your data to beautiful graphs and charts with just a few clicks.

### Features

* Pulses - Schedule and send charts or results to your team via email or Slack
* Dashboards - Let everyone on your team create, organize, and share beautiful collections of visualizations and data.
* Open source and constantly improving.

